import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, FlatList,Dimensions, Image, View, TouchableOpacity} from 'react-native';
import Axios from 'axios';
import { connect } from 'react-redux'
import Navbar from '../components/navbar';
import Footer from '../components/footer'; 
import EventList from '../components/eventList'; 
import KategoriList from '../components/kategoriList'; 
import storeCredential,{actionCreators}  from '../reducer/credentialRedux';
import { PacmanIndicator } from 'react-native-indicators';

const mapStateToProps = (state) => ({
    dataWorkshop: state.dataWorkshop,
    dataKursus: state.dataKursus,
    dataSeminar: state.dataSeminar,
    dataKategori: state.dataKategori,
    isLoading: state.isLoading,
    isError: state.isError,
})

class home extends Component  {
    // Mount Fungsi Get
    componentDidMount() {
        storeCredential.dispatch(actionCreators.isLoading(true));
        storeCredential.dispatch(actionCreators.isError(false));
        this.getWorkshop(),
        this.getKursus(),
        this.getSeminar(),
        this.getKategori()
    }

    // Get Data Workshop
    getWorkshop = async () => {
        try {
            const response = await Axios.get(`https://inkubiz.id/on/api/sinau/event/workshop`)
            storeCredential.dispatch(actionCreators.dataWorkshop(response.data.data));
            storeCredential.dispatch(actionCreators.isError(false));
            storeCredential.dispatch(actionCreators.isLoading(false));
        } 
        catch (error) {
            storeCredential.dispatch(actionCreators.isError(true));
            storeCredential.dispatch(actionCreators.isLoading(false));
        }
    }

    // Get Data Kursus
    getKursus = async () => {
        try {
            const response = await Axios.get(`https://inkubiz.id/on/api/sinau/event/kursus`)
            storeCredential.dispatch(actionCreators.dataKursus(response.data.data));
            storeCredential.dispatch(actionCreators.isError(false));
            storeCredential.dispatch(actionCreators.isLoading(false));
        } 
        catch (error) {
            storeCredential.dispatch(actionCreators.isError(true));
            storeCredential.dispatch(actionCreators.isLoading(false));
        }
    }

    // Get Data Seminar
    getSeminar = async () => {
        try {
            const response = await Axios.get(`https://inkubiz.id/on/api/sinau/event/seminar`)
            storeCredential.dispatch(actionCreators.dataSeminar(response.data.data));
            storeCredential.dispatch(actionCreators.isError(false));
            storeCredential.dispatch(actionCreators.isLoading(false));
        } 
        catch (error) {
            storeCredential.dispatch(actionCreators.isError(true));
            storeCredential.dispatch(actionCreators.isLoading(false));
        }
    }

    // Get Data Kategori
    getKategori = async () => {
        try {
            const response = await Axios.get(`https://inkubiz.id/on/api/sinau/kategori-event-home`)
            storeCredential.dispatch(actionCreators.dataKategori(response.data.data));
            storeCredential.dispatch(actionCreators.isError(false));
            storeCredential.dispatch(actionCreators.isLoading(false));
        } 
        catch (error) {
            storeCredential.dispatch(actionCreators.isError(true));
            storeCredential.dispatch(actionCreators.isLoading(false));
        }
    }
  
    render(){
        //  If load data
        if (this.props.isLoading) {
            return (
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
                <PacmanIndicator size={80} color='#0061FF' />
            </View>
            )
        }
        // If data not fetch
        else if (this.props.isError) {
            return (
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
                <Text>Terjadi Error Saat Memuat Data</Text>
            </View>
            )
        }
      
        // If data finish load
        return (
            <View style={styles.container}>
                <Navbar navAction={()=>this.props.navigation.toggleDrawer()} />
                <ScrollView 
                vertical
                showsVerticalScrollIndicator={false}>
                    <View style={styles.headerContainer}>
                        <View style={styles.rowMargin}>     
                            <Text style={styles.textHeader}>Temukan (Seminar/Workshop/Kursus) Yang Anda Minati Disini</Text>
                                <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={this.props.dataKategori}
                                renderItem={(list)=><KategoriList data={list.item}/>}
                                keyExtractor={({ id }, index) => index.toString()}
                                />
                        </View>
                    </View>
                    <View style={styles.contentContainer}>
                        <Text style={styles.textHeader}>Seminar Online</Text>
                            <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={this.props.dataSeminar}
                            renderItem={(list)=><EventList data={list.item} title="Seminar Online" openDetail={() => this.props.navigation.push('Detail', {slug: list.item.slug})}/>}
                            keyExtractor={({ id }, index) => index.toString()}
                            />
                    </View>
                    <View style={styles.contentContainer}>
                        <Text style={styles.textHeader}>Workshop Online</Text>
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={this.props.dataWorkshop}
                            renderItem={(list)=><EventList data={list.item} title="Workshop Online"  openDetail={() => this.props.navigation.push('Detail', {slug: list.item.slug})}/>}
                            keyExtractor={({ id }, index) => index.toString()}
                            />
                    </View>
                    <View style={styles.contentContainer}>
                        <Text style={styles.textHeader}>Webinar</Text>
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={this.props.dataKursus}
                            renderItem={(list)=><EventList data={list.item} title="Seminar Online"  openDetail={() => this.props.navigation.push('Detail', {slug: list.item.slug})}/>}
                            keyExtractor={({ id }, index) => index.toString()}
                        />
                    </View>
                    <Footer/>
                </ScrollView>
            </View>
          );
    }
}
export default connect(mapStateToProps)(home)
const WIDTH = Dimensions.get('window').width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E0F9FE',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1, 
        padding: 20
    },
    aboutContainer: {
        alignItems: 'center',
        backgroundColor: '#F0F9F8',
        justifyContent: 'center',
        padding: 20,
        marginTop: 40
    },
    contentContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20
    },
    rowMargin:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    textHeader:{ 
        fontSize: 26, 
        color: "#0061FF",
        textAlign:'center',
        padding: 20,
        fontWeight:'bold'
    },
    headerImageContent: {
        width: '100%',
        height: 186,
        borderTopLeftRadius:20,
        borderTopRightRadius:20
    }
});
